#!/usr/bin/env bash
set -e

sudo mkdir /var/lib/docker-registry

cat <<EOF | sudo tee -a >/var/lib/docker-registry/config.yml
version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: /var/lib/registry
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3
proxy:
  remoteurl: https://registry-1.docker.io

EOF

docker-compose up -d

curl http://localhost:5000/v2/_catalog

cat <<JSON
{
    "registry-mirrors": ["http://localhost:5000"]
}
JSON

sudo systemctl restart docker
