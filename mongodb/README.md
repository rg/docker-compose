# Docker Mongo

A simple setup to test dump & restore before migration

Migration itself

```
mongodump --archive=staging-manager.archive
scp ... ...
mongorestore --archive=staging-manager.archive
```

