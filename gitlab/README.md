# Gitlab

Restoring data from an existing Gitlab, to test stuff like Gitlab LDAP auth

- Make sure docker-compose image's version match your Gitlab server's
- Start `docker-compose up -d`
- Wait a bit then http://localhost:10080 and set up a password for admin user `root`
- Stop `docker-compose stop`
- Backup your data on your gitlab server, `gitlab-rake gitlab:backup:create`
- Transfer it `rsync -avz your_gitlab_server:/var/opt/gitlab/backups volumes/gitlab/var/opt/gitlab/backups`
- Fix ownership `sudo chown 998:998 volumes/gitlab/var/opt/gitlab/backups/*`
- Transfer secrets `rsync -avz your_gitlab_server:/etc/gitlab/*secret* volumes/gitlab/etc/gitlab/`
- Fix ownership `sudo chown root: volumes/gitlab/etc/gitlab/*`
- Start again `docker-compose up -d`
- Wait for login page to be available again
- Start restoration `docker-compose exec gitlab gitlab-rake gitlab:backup:restore`
