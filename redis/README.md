# Docker Redis

A simple setup to test redis migration

Admin WebUI will be available at http://localhost:8081

## Cold migration

```
redis-cli info
redis-cli config get dir
redis-cli shutdown

scp /path/to/dump.rdb new_server:path/to/dump.rdb
```

## Hot migration

Old redis server is 10.10.10.10 port 1234 with password blablablablabla.

New server is 10.35.123.234. Start it then `redis-cli` :

```
redis 127.0.0.1:8888> slaveof 10.10.10.10 1234
OK

redis 127.0.0.1:8888> config set masterauth "blablablabla"
OK

redis 127.0.0.1:8888> info
...
role:slave
master_host:127.0.0.1
master_port:1234
master_link_status:up
master_last_io_seconds_ago:1
master_sync_in_progress:0
...
```

Check info for `master_sync_in_progress`

Once done, update application to the new redis

Then cut it off from the old `redis-cli slaveof no one`
