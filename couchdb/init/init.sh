#!/usr/bin/env bash
set -Eeuxo pipefail

PROTOCOL=${COUCHDB_PROTOCOL:-"http"}
HOST=${COUCHDB_HOST:-"couchdb"}
PORT=${COUCHDB_PORT:-"5984"}
ADMIN=${COUCHDB_ADMIN:-"admin"}
PASSWORD=${COUCHDB_PASSWORD:-"couchdb"}

# Admin user
# curl -X PUT "http://$HOST:$PORT/_config/admins/$ADMIN" -d "$PASSWORD"

# Databases
for db in _users _metadata _replicator _global_changes; do
  curl -sSkL -X PUT "${PROTOCOL}://${ADMIN}:${PASSWORD}@${HOST}:${PORT}/${db}"
done
