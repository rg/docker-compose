# Docker CouchDB

Basic docker-compose setup for CouchDB cluster + Fauxton webUI

```
docker-compose up -d
```

WebUI : http://localhost:8000
